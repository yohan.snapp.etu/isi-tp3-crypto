import utils.usb as usb
import utils.path as path
import utils.my_crypt as my_crypt
import os
import sys

def generate_all_crypted_master_key(usb_path_new_responsive, role, id, usb_paths) :
    for usb_path in usb_paths : 
        if os.path.exists(usb_path) :
            # On crée le liens où sauvegarder la nouvelle version de notre MASTER_KEY
            path_for_key = path.get_path_crypted_master_key(usb.get_id(usb_path), id)
            # On  récupere la MASTER_KEY 
            master_key = my_crypt.load_key(path.MASTER_KEY)
            # On recupere la cprésenter sur usb_path
            decrypted_manager_key = my_crypt.decrypt_data(usb.read_key(usb_path), path.KEY_RESP).encode()
            # On crée un clé pour le nouveau manager
            key_new_manager = my_crypt.generate_key()
            # On crypte notre MASTER_KEY, le manager avec l'id le plus faible  en premier 
            if usb.get_id(usb_path) < id :
                master_key = my_crypt.encrypt_data_with_given_key(master_key, decrypted_manager_key)
                master_key = my_crypt.encrypt_data_with_given_key(master_key, key_new_manager)
            else :
                master_key = my_crypt.encrypt_data_with_given_key(master_key, key_new_manager)
                master_key = my_crypt.encrypt_data_with_given_key(master_key, decrypted_manager_key)
            # On stock notre nouvelle version cryptée de la master key
            my_crypt.write_key(master_key, path_for_key)
            # On crypt et ecrit la clé sur l'usb de notre nouveau manager 
            my_crypt.write_key(my_crypt.encrypt_data(key_new_manager, path.KEY_RESP), path.get_path_usb_key(usb_path_new_responsive))

def main(argv) :
    if len(argv) >= 3:
        print("===== Adding a new legal representative ===")
        id = usb.init_usb(argv[0], argv[1])
        generate_all_crypted_master_key(argv[0], argv[1], id, argv[2:])
    else : 
        print("Usage : python3 scripts/delete_pair.py [usb_path_new_responsive] [role] [usb_paths..]")
        print("With [usbs_path] all other usb which will be able to lunch server in association with our new responsive")                                     


if __name__ == "__main__":
    main(sys.argv[1:])