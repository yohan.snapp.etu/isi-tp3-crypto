import utils.managers as managers
import utils.my_crypt as my_crypt
import utils.path as path
from getpass import getpass
import sys

def revocation(id_to_revocate) :
    db_managers = managers.select_all()
    for manager in db_managers :
        if manager[0] != id_to_revocate :
            attempt = 0
            ask = True
            while ask : 
                input_login = input("Login for manager with id " + str(manager[0]) + ' : ')
                if managers.check_login(manager[0], input_login) :
                    input_password = my_crypt.encrypt_data(getpass("Password : "), path.KEY_PASSWORD)
                    if managers.check_password(manager[0], input_password) :
                        ask = False
                attempt = attempt + 1
                if attempt == 5 :
                    sys.exit("= Authentication failed =")
    managers.delete_manager(id_to_revocate)
    print("== Succesfull revocation =")   
     
def main(argv) :
    if len(argv) >= 1 : 
        print("======== Revocate ========")
        
        revocation(argv[0])
    else : 
        print("usage : python3 scripts/revocation.py [id]")                                     


if __name__ == "__main__":
    main(sys.argv[1:])