"""
Module to memorize the paths used in this project 
"""
import pathlib

# Manager 
ROOT_PROJECT = str(pathlib.Path(__file__).parents[2])
# MASTER KEY
MASTER_KEY =  ROOT_PROJECT  + "/ramdisk/keys/MASTER_KEY.key"
# Crypted MASTER_KEY
DD_KEYS_DIR =  ROOT_PROJECT  + "/disks/dd/keys/"
# KEY_RESP
KEY_RESP =  ROOT_PROJECT + "/disks/dd/keys/KEY_RESP.key"
# KEY_PASSWORD
KEY_PASSWORD = ROOT_PROJECT + "/disks/dd/keys/KEY_PASSWORD.key"
# PART_KEY
USB0 = ROOT_PROJECT + "/disks/usb0/"
USB1 = ROOT_PROJECT + "/disks/usb1/"
# Database
DATABASE_CARD = ROOT_PROJECT + "/disks/dd/database/db_card"
DATABASE_MANAGERS = ROOT_PROJECT + "/disks/dd/database/db_managers"
# NB USB
NB_USB = ROOT_PROJECT + "/disks/dd/database/nb_usb"
#Repudiate
REPUDIATE = ROOT_PROJECT + "/disks/dd/database/repudiate"

def get_path_usb_id(path_usb) :
    """
    Give the path for the file which containe usb's id
    """
    return path_usb + "/.id"

def get_path_usb_key(path_usb) :
    """
    Give the path for the file which containe usb's KEY.key 
    """

    return path_usb + "/.KEY.key"

def get_path_crypted_master_key(id_usb1, id_usb2) :
    """
    Give the path for the file which containe the crypted master key associated to the both managers given as param.
    """

    if id_usb1 < id_usb2 :
        str_id = str(id_usb1) + '_' + str(id_usb2)
    else : 
        str_id = str(id_usb2) + '_' + str(id_usb1)
    return DD_KEYS_DIR +  "CRYPTED_MASTER_KEY_" + str_id + ".key"
    
