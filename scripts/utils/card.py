"""
Module to interact with db_card
"""
import utils.my_crypt as my_crypt
import utils.path as path

def insert_pair(name, num_card) :
    """
    Add pair in the card database
    """
    with open(path.DATABASE_CARD, "a") as f :
        f.write(name + ";" + my_crypt.encrypt_data(num_card, path.MASTER_KEY).decode() + ';\n')

def delete_pair(name, num_card) :
    """
    Delete the wanted pair in the card database
    """
    with open(path.DATABASE_CARD, "r") as f:
        lines = f.readlines()
    with open(path.DATABASE_CARD, "w") as f:
        for line in lines:
            if line.startswith(name) :
                splitted_line = line.split(';') 
                card = my_crypt.decrypt_data(splitted_line[1].encode(), path.MASTER_KEY)
                if card != num_card :
                    f.write(line)
            else  :
                f.write(line)

def select_cards_by_name(name) :
    """
    Search for all cards associated to the given name
    """
    with open(path.DATABASE_CARD, "r") as f: 
        cards = []
        for line in f:
            if line.startswith(name):
                splitted_line = line.split(';')
                cards.append(my_crypt.decrypt_data(splitted_line[1].encode(), path.MASTER_KEY))
        return cards