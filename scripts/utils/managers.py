"""
Module to handle manager and interect with db_manager
"""
import utils.path as path
import utils.my_crypt as my_crypt
import sys

def insert_manager(id, login, crypted_password, role) :
    """
    Insert a manager in the database of manager
    """
    if role != "legal" and role != "technical" :
        sys.ecit("Role must be legal or technical")
    db_managers = select_all()
    for manager in db_managers : 
        if manager[0] == str(id) :
            sys.exit("Id must be unique")
        if manager[1] == login :
            sys.exit("Login must be unique")
    to_write = str(id) + ';' + login + ';' + crypted_password.decode()  + ';' + role + ';' + '\n'
    with open(path.DATABASE_MANAGERS, "a") as f :
        f.write(to_write)
    
def delete_manager(id) :
    """
    Delete a manager in the database of manager
    """
    db_managers = select_all()
    with open(path.DATABASE_MANAGERS, "w") as f :
        for managers in db_managers : 
            if managers[0] != id : 
                to_write = managers[0] + ';' + managers[1] + ';' + managers[2]  + ';' + managers[3] + ';' + '\n'
                f.write(to_write)
    
def select_managers_by_id(id) :
    """
    Select manager with given id
    """
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[0] == str(id) :
                return splitted_line

def select_managers_by_login(login) :
    """
    Select manager with given login
    """
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[1] == login :
                return splitted_line

def select_managers_by_role(role) :
    """
    Select all manager with the given role
    """
    selected_managers = []
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[3] == role :
                selected_managers.append(splitted_line)
    return splitted_line

def select_all() :
    """
    Return all the manager's database
    """
    db_managers = []
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            db_managers.append(splitted_line)
    return db_managers

def check_password(id, crypted_password) : 
    """
    Check if password given as parameter is the same in the database
    """
    manager = select_managers_by_id(id)
    if my_crypt.decrypt_data(manager[2].encode(), path.KEY_PASSWORD) == my_crypt.decrypt_data(crypted_password, path.KEY_PASSWORD): 
        return True
    else :
        print("Wrong password, try again")
        return False

def check_login(id, login) :
    """
    Check if login given as parameter correspond to any authorized manager
    """
    manager = select_managers_by_id(id)
    if manager[1] == login : 
        return True
    else :
        print("Wrong login")
        return False

def check_if_id_is_associated_to_manager_with_rights(id) :
    """
    Check if the given id is associated to a manager wich have rights to restart server
    """
    managers = select_all()
    for manager in managers :
        if manager[0] == str(id) : 
            return True
    print("This id does not match with any authorized manager")
    return False

def get_manager_role(id) :
    """
    Get the role of the manager with the diven id
    """
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[0] == str(id) :
                return splitted_line[3]
