"""
Module for crypting using fernet.
"""
from cryptography.fernet import Fernet
import sys

def generate_key() :
    """
    Generate a key
    """
    key = Fernet.generate_key()
    return key

def write_key(key, path) :
    """
    Write given key into given file 
    """
    with open(path, "wb") as key_file:
        key_file.write(key)


def load_key(path) :
    """
    Load the key located at the given path
    """
    try :
         return  open(path, "rb").read()
    except FileNotFoundError :
        sys.exit("Your system must be started")
 

def encrypt_data(data, key_path) :
    """
    Encrypts a data with the key located at the given as parameter
    """
    key = load_key(key_path)
    if type(data) != bytes :
        data = data.encode()
    f = Fernet(key)
    encrypted_data = f.encrypt(data)
    return encrypted_data

def encrypt_data_with_given_key(data, key) :
    """
    Encrypts a data with with key given as parameter
    """
    if type(data) != bytes :
        data = data.encode()
    f = Fernet(key)
    encrypted_data = f.encrypt(data)
    return encrypted_data

def decrypt_data(data, key_path) :
    """
    Decrypts data 
    """
    key = load_key(key_path)
    f = Fernet(key)
    decrypted_data = f.decrypt(data)
    return (decrypted_data.decode()) 

def decrypt_data_with_given_key(data, key) :
    """
    decrypt a data with key given as parameter
    """
    f = Fernet(key)
    decrypted_data = f.decrypt(data)
    return (decrypted_data.decode())