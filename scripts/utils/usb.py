"""
Module to handle usb init
"""

import utils.my_crypt as my_crypt
import utils.path as path
import utils.managers as managers
from getpass import getpass
    
def read_nb_usb() :
    """
    Get in the disk the current number of usb already created.
    """
    try :
        with open(path.NB_USB, "r+") as f :
            return int(f.read())
    except FileNotFoundError :
        print("Your system must be initiated with the script init")

def update_nb_usb(new_n) :
    """
    Update the number of usb created.
    """
    with open(path.NB_USB, 'w') as f :
        f.write(str(new_n))

def set_id(id, path_usb) :
    """
    Set id of the usb located at the given path.
    """
    path_rank = path.get_path_usb_id(path_usb)
    with open(path_rank, "w+") as f :
        f.write(str(id))

def get_id(path_usb) :
    """
    Get the id of the usb located at the given path.
    """
    path_rank = path.get_path_usb_id(path_usb)
    try :
        with open(path_rank, "r") as f :
            return int(f.read())
    except FileNotFoundError :
        print("Usb must be initiated.")

def create_password() :
    """
    Ask user for creating a new password .
    """
    return my_crypt.encrypt_data(getpass("Password for usb : "), path.KEY_PASSWORD)
     
def create_login() :
    """
    Ask user for creating a new password .
    """
    return input("Choose a login : ")

def read_key(path_usb) :
    """
    Read the part_of_key of the usb located at the given path.
    """
    path_key = path.get_path_usb_key(path_usb)
    with open(path_key, "rb") as f :
        return f.read()

def init_usb(path_usb, role) :
    """
    Initiate the usb located at the given path, with the  given role.
    """
    id = read_nb_usb()
    set_id(id, path_usb)
    login = create_login()
    password = create_password()
    managers.insert_manager(id, login, password, role)
    update_nb_usb(read_nb_usb() + 1)
    return id