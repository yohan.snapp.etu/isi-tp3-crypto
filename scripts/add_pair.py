import utils.card as card
import sys

def main(argv) :
    if len(argv) >= 2: 
        print("===== Adding a new pair ===")
        card.insert_pair(argv[0], argv[1])
    else : 
        print("Usage : python3 scripts/delete_pair.py [name] [n°]")                                     


if __name__ == "__main__":
    main(sys.argv[1:])