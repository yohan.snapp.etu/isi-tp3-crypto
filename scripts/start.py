import utils.my_crypt as my_crypt
import utils.path as path
import utils.usb as usb
import utils.managers as managers
import sys
from getpass import getpass

def check_roles(id1, id2) :
    """
    Verify if the both legal and technical manager are here.
    """
    if managers.check_if_id_is_associated_to_manager_with_rights(id1) and managers.check_if_id_is_associated_to_manager_with_rights(id2) :
        role1 = managers.get_manager_role(id1)
        role2 = managers.get_manager_role(id2)
        if role1 != role2 :
            if (role1 == "legal" or role2 == "legal") and (role1 == "technical" or role2 == "technical") :
                return True 
        print("There must be both legal and technical manager")
    return False

def authentification(id) :
    """
    Check if the manager still have the rights for strating server then ask and verify manager's password
    """
    attempt = 0
    while attempt < 5 : 
        input_login = input("Login for manager with id " + str(id) + ' : ')
        if managers.check_login(id, input_login) :
            input_password = my_crypt.encrypt_data(getpass("Password : "), path.KEY_PASSWORD)
            if managers.check_password(id, input_password) :
                return True
        attempt = attempt + 1
    print("Too much wrong password for usb" + str(id))
    return False

def load_master_key(usb_path1, usb_path2, path_crypted_mk) :
    """
    Decrypt the crypted version of MASTER_KEY and write it in the ram_disk
    """

    # Recuper la MASTER_KEY cryptée
    master_key = my_crypt.load_key(path_crypted_mk)

    # Effectue le double decryptage
    decrypted_usb_key1 = my_crypt.decrypt_data(usb.read_key(usb_path1), path.KEY_RESP).encode()
    decrypted_usb_key2 = my_crypt.decrypt_data(usb.read_key(usb_path2), path.KEY_RESP).encode()
    # On effectue le double decryptage, l'id le plus grand dcrypte en premier 
    if usb.get_id(usb_path1) > usb.get_id(usb_path2) : 
        master_key = my_crypt.decrypt_data_with_given_key(master_key, decrypted_usb_key1)
        print(master_key)
        master_key = my_crypt.decrypt_data_with_given_key(master_key.encode(), decrypted_usb_key2)
    else : 
        master_key = my_crypt.decrypt_data_with_given_key(master_key, decrypted_usb_key2)
        master_key = my_crypt.decrypt_data_with_given_key(master_key.encode(), decrypted_usb_key1)
    # Ecrit la MASTER_KEY dans la ramdisk
    my_crypt.write_key(master_key.encode(), path.MASTER_KEY)

def main(argv) :
    if len(argv) >= 2: 
        print("=========== Starting server =========")
        id1 = usb.get_id(argv[0])
        id2 = usb.get_id(argv[1])
        if check_roles(id1, id2) :
            if authentification(id1) and authentification(id2) :
                print("===== Successful autehntication =====")
                # Recupere le chemin de notre MASTER_KEy cryptée
                path_crypted_mk = path.get_path_crypted_master_key(id1, id2)
                load_master_key(argv[0], argv[1], path_crypted_mk)
                print("==== MASTER_KEY loaded in ramdisk ===")
    else : 
        print("Usage : python3 scripts/start.py [path_usb1] [path_usb2]")                            

if __name__ == "__main__":
    main(sys.argv[1:])
