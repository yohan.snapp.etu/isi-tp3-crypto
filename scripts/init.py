import utils.my_crypt as my_crypt
import utils.path as path
import utils.usb as usb
import os

def init():
    """
    Init our system
    """
    print("================ Init server ===============")
    # Vide notre database de responsable et notre database de carte
    with open(path.DATABASE_MANAGERS, "w") as f :
        f.write('')
    with open(path.DATABASE_CARD, "w") as f :
        f.write('')

    # On genere KEY_PASSWORD et l'ecrit dans le fichier associé
    key_password = my_crypt.generate_key() 
    my_crypt.write_key(key_password, path.KEY_PASSWORD)

    # On init les clés usb 
    usb.update_nb_usb(0)
    print("========== Init key legal manager ==========")
    usb.init_usb(path.USB0, "legal")
    print("========== Init key technical manager ======")
    usb.init_usb(path.USB1, "technical")

    # On genere KEY_RESP l'ecrit dans le fichier associé
    key_password = my_crypt.generate_key() 
    my_crypt.write_key(key_password, path.KEY_RESP)

    # On crée un clé pour usb0
    key_usb0 = my_crypt.generate_key()
    
    # On crée un clé pour usb1
    key_usb1 = my_crypt.generate_key()

    # On crée notre MASTER_KEY 
    master_key = my_crypt.generate_key()

    # On realise le double cryptage de la MASTER_KEY
    master_key = my_crypt.encrypt_data_with_given_key(master_key, key_usb0)
    master_key = my_crypt.encrypt_data_with_given_key(master_key, key_usb1)

    # On crypte les clés des usb puis les stock 
    my_crypt.write_key(my_crypt.encrypt_data(key_usb0, path.KEY_RESP), path.get_path_usb_key(path.USB0))
    my_crypt.write_key(my_crypt.encrypt_data(key_usb1, path.KEY_RESP), path.get_path_usb_key(path.USB1))

    # On recupere le path ou stocker la MASTER_KEY cryptée et on ecrit dans ce fichier
    path_crypted_mk = path.get_path_crypted_master_key(usb.get_id(path.USB0), usb.get_id(path.USB1))
    my_crypt.write_key(master_key, path_crypted_mk)

def main() :
    init()

if __name__ == "__main__":
    main()