import utils.card as card
import sys

def main(argv) :
    if len(argv) >= 1: 
        print("===== Searching cards ===")
        selected_cards = card.select_cards_by_name(argv[0])
        print("Card for : " + argv[0] + ' :')
        for i in range (len(selected_cards)) :
            print('\t' + str(i) +  " : " +  selected_cards[i])
    else : 
        print("Usage : python3 scripts/delete_pair.py [name]")                                   


if __name__ == "__main__":
    main(sys.argv[1:])