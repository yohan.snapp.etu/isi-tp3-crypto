# TP 3 - Crypto  

## Auteur

* Paul **Lerat-Lambert**, *paul.leratlambert.etu@univ-lille.fr*
* Yohan **Snapp**, *yohan.snapp.etu@univ-lille.fr*

## Partie 1 : Responsabilité partagée.  

### Question 1 :  

* La clé de cryptage principale, `MASTER_KEY`, est stocké sur notre serveur de façon cryptée. Ce cryptage est en réalité un cryptage double réalisé par deux clés différentes : `KEY_USB1` et `KEY_USB2`.
* Chaque responsable possède une clé USB sur laquelle est gardé une version cryptée par le `KEY_RESP` de ces clés. 
* Lorsque l'on veut récupérer la `MASTER_KEY` les deux responsables doivent alors insérer leurs cés USB dans le système puis s'authentifier.
* Une fois l'authentification réussié on decrypte les clés presente sur les clées usb, on posséde désormais les deux clés `KEY_USB1` et `KEY_USB2`
* Grace à ces clés on est capable de décrypter la `MASTER_KEY`
* Le fichier `disks/dd/database/db_managers` garde en mémoire les responsable ayant le droit de redémarrer le serveur, sur ce fichier les mots de passe sont cryptés grace à la clé `KEY_PASSWORD`

### Question 2 :  

Pour le développement de cette **PoC** nous avonas impléementer 5 modules présent dans le dossier `scripts/utils` :
    * **card :** S'occupe de la communication avec la base de données stockant les pairs : **nom;n° de carte**.
    * **managers :** S'occupe de la communication avec la base de données stockant les pairs : **id;login;password;role**.
    * **my_crypt :** Différente methode pour crypter / decrypter des données.
    * **path :** Garde en mémoire les différents chemins vers nos clés, ainsi que queluqes methodes pour générer des chemins nécessaire au bon fonctionnement du serveur.
    * **usb :** S'occupe de l'initialisation de l'usb, deplus certaines methodes permettent d'ecrire/Lire les données présente sur les clés usb.

#### Initalisation

Le script permetant l'initalistion du serveur est implémenté dans le fichier `scripts/init.py il s'utilise de la maniere suivante :
```bash
python3 scripts/init.py
```

#### Mise en route du serveur

Le script permetant la mise en route du serveur est implémenté dans le fichier `scripts/start.py il s'utilise de la maniere suivante :
```bash
python3 scripts/start.py [path_usb1] [path_usb2]
```

#### Ajouter une pair

Le script permetant l'ajout d'une pair est implémenté dans le fichier `scripts/add_pair.py il s'utilise de la maniere suivante :
```bash
python3 scripts/add_pair.py [nom] [n°]
```

#### Supprimer une pair

Le script permetant la suppression d'une pair est implémenté dans le fichier `scripts/delete_pair.py il s'utilise de la maniere suivante :
```bash
python3 scripts/delete_pair.py [nom] [n°]
```

#### Chercher numéros de cartes

Le script permetant la recherche de numéros de carte est implémenté dans le fichier `scripts/search_card.py il s'utilise de la maniere suivante :
```bash
python3 scripts/search_card.py [nom]
```

## Partie 2 : Délégation de droit

### Question 3 :  

* Considérons :
    * **R0** résponable téchnique et **R1** résponsable juridique.
    * **R2** représentant légal de **R0**.
    * **R3** représentant légal de **R1**.
    
* Il existe alors 4 couples capable d'obtenir la `MASTER_KEY` :
    * **R0 + R2**
    * **R0 + R3**
    * **R1 + R2**
    * **R2 + R3**

* L'idée est alors de garder sur le serveur quatre version cryptée différamment de la `MASTER_KEY` :
    * `CRYPTED_MASTER_KEY_0_1`
    * `CRYPTED_MASTER_KEY_0_3`
    * `CRYPTED_MASTER_KEY_1_2`
    * `CRYPTED_MASTER_KEY_2_3`

### Question 4 :

Le script permetant l'ajout d'un réprésentant est implémenté dans le fichier `scripts/add_legal_representative.py` il s'utilise de la maniere suivante :
```bash
python3 scripts/delete_pair.py [usb_path_new_responsive] [role] [usb_paths..]
```
Avec `usb_path` toutes les autres usb avec qui le nouveau responsable sera capable de lancer le serveur.


## Partie 3 : Révocation de droit

### Question 5 :

* Lorsque l'on veut révoquer un responsable, on doit pouvoir identifier tous les autres.  
* Pour ce faire, on pourrait demander aux 3 autres de se connecter grâce à leur couple Login/Password. Par exemple, si on veut révoquer le **R2**, il faut que **R0**, **R1** et **R3**  s'identifient ensemble.
* Une fois que tous les ayant droit sont connectés, une procédure de suppression est lancé et ceux qui ne sont pas connecté sont viré.

### Question 6 :

Le script permetant l'ajout d'un réprésentant est implémenté dans le fichier `scripts/revocation.py` il s'utilise de la maniere suivante :
```bash
python3 scripts/revocation.py [id]
```
