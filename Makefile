all : init start add_pair search_card delete_pair search_card add_legal_representative revocation

init : 
	python3 scripts/init.py

start : 
	python3 scripts/start.py disks/usb0 disks/usb1

add_pair :
	python3 scripts/add_pair.py Lerat-Lambert 123456789
	python3 scripts/add_pair.py Lerat-Lambert 987654321
	python3 scripts/add_pair.py Snapp 123456789
	python3 scripts/add_pair.py Snapp 789654321

delete_pair : 
	python3 scripts/delete_pair.py Snapp 789654321

search_card :
	python3 scripts/search_card.py Lerat-Lambert
	python3	scripts/search_card.py Snapp

add_legal_representative :
	python3 scripts/add_legal_representative.py disks/usb3 technical disks/usb0

revocation :
	python3 scripts/revocation.py 2

clean :
	rm -rf disks/dd/database/*
	rm -rf disks/dd/keys/*
	rm -rf disks/usb0/.KEY.key
	rm -rf disks/usb0/.id
	rm -rf disks/usb1/.KEY.key
	rm -rf disks/usb1/.id
	rm -rf disks/usb2/.KEY.key
	rm -rf disks/usb2/.id
	rm -rf disks/usb3/.KEY.key
	rm -rf disks/usb3/.id
	rm -rf ramdisk/keys/*